# Lab 5 - Roles
## Objectif 

Convertir le playbook du lab 3 en deux rôles

- Créez deux rôles : commun et apache 
- Créez un playbook pour appliquer ces rôles.

« commun » devrait être appliqué à tous les serveurs Linux

« apache » devrait être appliqué à votre groupe « Web »

- Mettez les template jinja2 dans le dossier approprié
